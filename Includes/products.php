<?php
require_once("jtdb.php");

abstract class Product
{
    private $sku,
            $name,
            $price,
            $productType,
            $size,
            $weight,
            $height,
            $width,
            $length;

    public function __construct($postArray) {
      $this->setSku($postArray["sku"]);
      $this->setName($postArray["name"]);
      $this->setPrice($postArray["price"]);
      $this->setProductType($postArray["productType"]);
    }

  	public function addProduct() {
        JtakDB::getInstance()->insert_product($this->getSku(), $this->getName(), $this->getPrice(), $this->getProductType(), $this->getSize(), $this->getWeight(), $this->getHeight(), $this->getWidth(), $this->getLength());
    }

    public abstract function getSpecificProperties(): string;

    public function setSku($sku) {
      $this->sku = htmlentities($sku);
    }
    public function getSku() {
      return $this->sku;
    }

    public function setName($name) {
      $this->name = htmlentities($name);
    }
    public function getName() {
      return $this->name;
    }

    public function setPrice($price) {
      settype($price, "float");
      $this->price = $price;
    }
    public function getPrice() {
      return $this->price;
    }

    public function setProductType($productType) {
      $this->productType = $productType;
    }
    public function getProductType() {
      return $this->productType;
    }

    public function setSize($size) {
      settype($size, "integer");
      $this->size = $size;
    }
    public function getSize() {
      return $this->size;
    }

    public function setWeight($weight) {
      settype($weight, "integer");
      $this->weight = $weight;
    }
    public function getWeight() {
      return $this->weight;
    }

    public function setHeight($height)
    {
      settype($height, "integer");
      $this->height = $height;
    }
    public function getHeight()
    {
      return $this->height;
    }

    public function setWidth($width)
    {
      settype($width, "integer");
      $this->width = $width;
    }
    public function getWidth()
    {
      return $this->width;
    }

    public function setLength($length)
    {
      settype($length, "integer");
      $this->length = $length;
    }
    public function getLength()
    {
      return $this->length;
    }
}

class Disc extends Product {

    public function __construct($postArray) {
      parent::__construct($postArray);
  	  $this->setSize($postArray["size"]);
    }

    public function getSpecificProperties(): string {
      return "Size: " . $this->getSize() . " MB";
    }
}

class Book extends Product {

    public function __construct($postArray) {
      parent::__construct($postArray);
      $this->setWeight($postArray["weight"]);
    }

    public function getSpecificProperties(): string {
      return "Weight: " . $this->getWeight() . " KG";
    }
}

class Furniture extends Product {

    public function __construct($postArray) {
      parent::__construct($postArray);
      $this->setHeight($postArray["height"]);
      $this->setWidth($postArray["width"]);
      $this->setLength($postArray["length"]);
    }

    public function getSpecificProperties(): string {
      return "Dimension: " . $this->getHeight() . "x" . $this->getWidth() . "x" . $this->getLength();
    }
}
?>