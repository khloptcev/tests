<?php
require_once("Includes/db_config.php");
class JtakDB extends mysqli {

    // single instance of self shared among all instances
    private static $instance = null;

    // This method must be static, and must return an instance of the object if the object
    // does not already exist.
    public static function getInstance() {

      if (!self::$instance instanceof self) {
        self::$instance = new self;
      }

      return self::$instance;
    }

    // The clone and wakeup methods prevents external instantiation of copies of the Singleton class,
    // thus eliminating the possibility of duplicate objects.

    public function __clone() {
      trigger_error('Clone is not allowed.', E_USER_ERROR);
    }

    public function __wakeup() {
      trigger_error('Deserializing is not allowed.', E_USER_ERROR);
    }

    public function __construct() {

      parent::__construct(DBHOST, DBUSER, DBPASS, DBNAME);

      if (mysqli_connect_error()) {
        exit('Connect Error (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
      }
      parent::set_charset('utf8mb4');
    }

    // Prevent SQL-injections
    public function insert_product($sku, $name, $price, $productType, $size, $weight, $height, $width, $length) {
      $sku = $this->real_escape_string($sku);
      $name = $this->real_escape_string($name);
      $productType = $this->real_escape_string($productType);

      $stmt = $this->prepare("INSERT INTO product_list VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
      $stmt->bind_param('issdsiiiii', $product_id, $c_sku, $c_name, $c_price, $c_productType, $c_size, $c_weight, $c_height, $c_width, $c_length);
      $c_sku = $sku;
      $c_name = $name;
      $c_price = $price;
      $c_productType = $productType;
      $c_size = $size;
      $c_weight = $weight;
      $c_height = $height;
      $c_width = $width;
      $c_length = $length;

      $stmt->execute();
    }

    public function get_products() {
      return $this->query("SELECT product_id, sku, name, price, productType, size, weight, height, width, length FROM product_list");
    }

    function delete_products ($products_ids) {
      $this->query("DELETE FROM product_list WHERE product_id IN($products_ids);");
    }
}
?>