<?php
  require_once("Includes/jtdb.php");
  require_once("Includes/products.php");
  session_start();
  if ($_SERVER['REQUEST_METHOD'] == "POST"){
    if (array_key_exists("save", $_POST)) {
      $productObject = NULL;
      switch ($_POST['productType']) {
        case "DVD":
            $productObject = new Disc($_POST);
            break;
        case 'Book':
            $productObject = new Book($_POST);
            break;
        case 'Furniture':
            $productObject = new Furniture($_POST);
            break;
      }
      $productObject->addProduct();
      header('Location: index.php' );
      exit;
    }
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <link href="./Includes/jtak.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
    <script defer src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script defer src="Includes/jtak.js"></script>
  </head>
  <body>
    <form id="product_form" action="add-product.php" method="POST">
      <div class="header_layout">
        <h1>Product Add</h1>
        <div class="header_layout">
          <div>
            <input type="submit" form="product_form" name="save" value="Save" />
          </div>
          <div class="right-button">
            <button class="btn-cancel" type="button" href="/index.php">Cancel</button>
          </div>
        </div>
      </div>
      <hr>
      <p class="flex3">
        <label class="form_label" for="sku">SKU</label>
        <input class="input_box2" type="text" id="sku" name="sku" minlength="2" maxlength="13" placeholder="sku (13)" required>
      </p>
      <p class="flex3">
        <label class="form_label" for="name">Name</label>
        <input class="input_box2" type="text" id="name" name="name" maxlength="50" placeholder="Name of product (50)" required>
      </p>
      <p class="flex3">
        <label class="form_label" for="price">Price ($)</label>
        <input class="input_box2" type="number" id="price" name="price" min="1" step="1" required >
      </p>

      <p class="flex3">
        <label class="form_label" for="type">Type switcher</label>
        <select class="input_box2" id="productType" name="productType" required>
          <option value="DVD">DVD</option>
          <option value="Book">Book</option>
          <option value="Furniture">Furniture</option>
        </select>
      </p>

      <div class="hide DVD">
        <div class="flex3">
          <label class="form_label" for="size">Size (MB)</label>
          <input class="input_box2" type="number" id="size" name="size" min="0" max="9626" step="1">
          <p class="item4">Please, provide size in MB</p>
        </div>
      </div>

      <div class="hide Book">
        <div class="flex3">
          <label class="form_label" for="weight">Weight (KG)</label>
          <input class="input_box2" type="number" id="weight" name="weight" min="1" max="200" step="1">
          <p class="item4">Please, provide weight in KG</p>
        </div>
      </div>

      <div class="hide Furniture">
        <div class="flex3">
          <label class="form_label" for="height">Height (CM)</label>
          <input class="input_box2" type="number" id="height" name="height" min="1" max="1500">
        </div>
        <p class="flex3">
        <label class="form_label" for="width">Width (CM)</label>
        <input class="input_box2" type="number" id="width" name="width" min="1" max="1500">
        </p>
        <div class="flex3">
        <label class="form_label" for="length">Length (CM)</label>
        <input class="input_box2" type="number" id="length" name="length" min="1" max="1500">
        </div>
        <p class="item4">Please, provide dimensions in HxWxL format</p>
      </div>
    </form>

    <script type="text/javascript">
      window.addEventListener('DOMContentLoaded', function() {
        var pType = "<?php echo $_POST['productType'] ?>";
        var select = document.querySelector('#productType'),
        hide = document.querySelectorAll('.hide');  // products fieldsets
        if (!pType=="") select.value=pType;
        function change()
        {
          hide.forEach.call(hide, function(el) {  // products fieldsets enum
            var add = el.classList.contains(select.value) ? "add" : "remove";
            el.classList[add]('show');
            var inputs = el.querySelectorAll('input');  // concrete product inputs
            if(add=="add") {
              inputs.forEach.call(inputs, function(el2) { // concrete product inputs enum (ADD requied)
                el2.setAttribute('required','');
              });
            } else {
              inputs.forEach.call(inputs, function(el22) { // concrete product inputs enum (REMOVE requied)
                if(el22.hasAttribute('required')) el22.removeAttribute('required');
                el22.value="";
              });
            }
          });
        };
        select.addEventListener('change', change);
        change();
      });
    </script>
  </body>
</html>