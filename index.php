<?php
    require_once("Includes/jtdb.php");
    require_once("Includes/products.php");
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <link href="./Includes/jtak.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
    <script defer src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script defer src="Includes/jtak.js"></script>
  </head>
  <body>
    <form id="add_item" action="add-product.php" method="POST"></form>
    <form id="delete_products" action="delete-products.php" method="POST">
      <div class="header_layout">
        <h1>Product List</h1>
        <div class="header_layout">
          <div>
            <input type="submit" form="add_item" value="ADD" />
          </div>
          <div class="right-button">
            <input id="delete-product-btn" type="submit" form="delete_products" value="MASS DELETE" />
          </div>
        </div>
      </div>
      <hr>
      <?php
        $products = JtakDB::getInstance()->get_products();
        while ($row = $products->fetch_array( MYSQLI_ASSOC )) :
          $product = NULL;
          $sku_class = NULL;
          switch ($row['productType']) {
            case "DVD":
                $product = new Disc($row);
                $sku_class = "prod_sku_dvd";
                break;
            case 'Book':
                $product = new Book($row);
                $sku_class = "prod_sku_book";
                break;
            case 'Furniture':
                $product = new Furniture($row);
                $sku_class = "prod_sku_furniture";
                break;
          }
     ?>
      <div class="list_layout">
        <input class="delete-checkbox" type="checkbox" name="toDeleteArray[]" value="<?php echo $row['product_id']; ?>"/>
        <span class="<?php echo $sku_class; ?>"><?php echo $product->getSku(); ?></span>
        <?php echo $product->getName(); ?><br />
        <?php echo $product->getPrice() . ' $'; ?><br />
        <?php echo $product->getSpecificProperties(); ?>
      </div>
      <?php
        endwhile;
        mysqli_free_result($products);
      ?>
    </form>
  </body>
</html>